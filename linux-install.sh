#!/bin/bash
echo "Installer for Eleos for ZClassic on Linux"
echo "This installation requires npm, node.js, unzip and wget to be installed before running"
read -n 1 -s -r -p "Press enter to continue if they are installed or Ctrl-C if you need to do this first"

if [ -f zcld-linux.zip ]; then
    rm zcld-linux.zip
    rm zcl-cli
    rm zcld-linux
echo "Downloading ZClassic full node binary"
    wget https://github.com/HarrKxx/eleos/releases/download/0.1.0/zcld-linux.zip
    unzip zcld-linux.zip
    chmod +x zcld-linux zcl-cli
fi

if [ -d $HOME/.zclassic/zclassic.conf ]; then
    echo "Removing probably incorrect zclassic.conf from pre-existing .zclassic configuration directory"
    rm $HOME/.zclassic/zclassic.conf
fi

if [ -d $HOME/.local/share/eleoszclassic ];then 
    rm -rf $HOME/.local/share/eleoszclassic
fi

mkdir $HOME/.local/share/eleoszclassic
cp assets/icons/zcl-icon.png $HOME/.local/share/icons/
cp -rfp * $HOME/.local/share/eleoszclassic/

echo "[Desktop Entry]">$HOME/.local/share/applications/eleoszclassic.desktop
echo "Name=Eleos ZClassic wallet">>$HOME/.local/share/applications/eleoszclassic.desktop
echo "Exec=$HOME/.local/share/eleoszclassic/linux-start.sh">>$HOME/.local/share/applications/eleoszclassic.desktop
echo "Icon=zcl-icon">>$HOME/.local/share/applications/eleoszclassic.desktop
echo "Type=Application">>$HOME/.local/share/applications/eleoszclassic.desktop
echo "Categories=zclassic;wallet;node;javascript">>$HOME/.local/share/applications/eleoszclassic.desktop
echo "Terminal=false">>$HOME/.local/share/applications/eleoszclassic.desktop
echo "StartupWMClass=eleos for zclassic">>$HOME/.local/share/applications/eleoszclassic.desktop

cp linux-eleos.desktop $HOME/.local/share/applications/eleoszclassic.desktop

chmod +x $HOME/.local/share/eleoszclassic/zcld-linux
chmod +x $HOME/.local/share/eleoszclassic/zcl-cli
chmod +x $HOME/.local/share/eleoszclassic/linux-start.sh

echo "Eleos for ZClassic now installed. You can launch it from your GUI now"
echo
echo "If Eleos for ZClassic gives an error about being unable to run the server,"
echo "run $HOME/.local/share/eleoszclassic/zcld-linux -reindex"
echo
echo "If you already had a zclassic wallet installed, you may need to reindex the database,"
read -n 1 -s -r -p "Press Enter to continue or Ctrl-C to abort"
echo "When reindexing is complete, press Ctrl-C to shut down the server and you will be able to run Eleos for ZClassic"
$HOME/.local/share/eleoszclassic/zcld-linux -reindex