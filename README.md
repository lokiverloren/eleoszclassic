# Eleos

Eleos is a wallet for zero-knowledge cryptocurrencies written in Electron.

* Compatible with Windows, MacOS, and Linux
* No third-party Java dependencies required
* Supports Zclassic

![Screenshot](https://pbs.twimg.com/media/DSgWlj_XkAAEz1B.jpg)

### Installation

Note: First time installations may take awhile to load since ~1GB of cryptographic data must be downloaded first.

##### Windows and MacOS

The simplest way to get started on Windows or MacOS is to [download and run the latest installer](https://github.com/HarrKxx/eleos/releases).

##### Linux

On linux, simply cd to this repository folder and run `./linux-install.sh` and everything will be done, including optional reindex of the zclassic blockchain should the wallet not start up normally.

This may happen if you have to force-reset your pc while the zcld-linux is running. Normally it won't be necessary.

Below is obsolete:

Note: Eleos requires that the compiled wallets are named zcld-linux and are saved into the eleos directory.
You can compile yourself or get the file here : https://github.com/HarrKxx/eleos/releases/tag/0.1.0

Get the source

```
git clone https://github.com/HarrKxx/eleos eleos
```

cd ~/Builds/eleos

```
npm install
```

Copy the Zclassic/Zcash wallet daemon into the elos directory (name the binary zcld-linux)

```
cp ~/Builds/zclassic/src/zcashd ~/Builds/eleos/zcld-linux
```

Start eleos

```
npm start
```

### Supported Wallets

Eleos is primarily designed for Zcash-based cryptocurrencies. The wallet.dat for each cryptocurrency is stored in the directories below.

| Support  | Status          | Windows Location   | MacOS Location                         |
| -------- | --------------- | ------------------ | -------------------------------------- |
| Zclassic | Fully supported | %APPDATA%/Zclassic | ~/Library/Application Support/Zclassic |

### Development

Want to contribute? Great! We need help.

### Donation

If you consider to make a donation, here are our addresses :
ZCL : t1LYLVqpy9eHwA5zaC3EoQnAsNwFAYDs35p

To donate to Loki for his work on this:

ZCL :
t1gyTSn1KqeZVMaDrEeD2J9hMNmnGspPWwt (transparent)

ZCL : zcQCAMwgDv3j7dnkKRu7YbRJNx3pzGmYxE1W9TKXN1tDhi89gzWXJRiRZGEPCgERUs7QYoSY4nsnFhmXtzFibmUCGXi4tD2 (private)

BTC : 34jQpjJku9VcMNLtfBc3qb13aAHeq5ji4U

ETH : 0x9934476f9d01d44156f89dde545f43bf466dc3eb

DASH : XsVWF7fzmRVYxFLKzv37xjha5QUvrvfBnJ

LTC : M9kjbYKkC51M4SWRnFPDGhgiAC6YXiLC7L

BCH : 1G9kj4jfkto1RFHXZUjkJ7m4p9siuJbjjR

Note for BTC, ETC, etc these are Bitpanda wallet addresses, very small donations may not be processed properly.

### Todos

* Import private key
* Import Wallet
* Choose fee
* Sign application & installer
* Refresh amount in tab after sending (need to wait confirmation ...)
* Throw error incorrect transaction
* Build for MacOs before Sierra
* Re-add Send Transparent
* Correct incompatibility with zcash
* Show all adress
* Write more documentation

### Help!

* Slack: get a login at https://discord.gg/SWzCnVD
* Other: Submit a Github issue as needed.

### License

Common Public Attribution License (CPAL-1.0)
Created by Josh Yabut for ZenCash
